var gulp = require('gulp');
var sass = require('gulp-sass');

function compilarSass() {
    return gulp
        .src('scss/bracket.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            Browserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
}

gulp.task('sass', compilarSass);

gulp.task('default', gulp.parallel('sass'));